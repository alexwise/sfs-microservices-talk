---
author: Aaron Brown and Alex Wise
title: Small Is Beautiful
subtitle: Microservices in Production
date: 2 September 2023
---
> "Make the right way the easy way"
~ Aaron Brown

---

> "Simplify and Standardize for Speed and Scale."
~ Aaron Brown

---

At FlowHub, we ran ~175 tracing-instrumented, event-driven microservices deployed to Kubernetes clusters in tiered environments via a push-based CICD pipeline that built and deployed on every commit.

What does that mean? Aaron will explain.

---

## The Architecture

![that's a fun image](images/architecture.png)

---

## Part 1: Make the right way the easy way

The power of sane defaults.

---

## Service Templates

Dev: "We need a new nodejs service with a REST API called "retro-encabulator."

Us: "Cool, here's a repo called "retro-encabulator" with a hello-world nodejs app. It has a REST API, our standard swagger specification, and standard logging and authentication libraries. It is wired up to our CICD pipeline and observability platform.

---

## values.yaml inheritance

Helm allows specifying values for your template that overwrite the default. 
This allows developers to only have to reason about where their app is different from the defaults.

- [The Full Template Defaults](assets/k8s-deploy-defaults.yaml)
- [The Service values.yaml file](assets/service-values.yaml)

---

## The k8s-deploy repository:

A repository of maintained and tested pipelines for deployment to our environments.

It contained common tests and deployment patterns as well.

The common pipeline code was inherited by the individual services in GitLab CICD.

---

## Example of pipeline inheritance

```
include:
  - project: 'Flowhub/k8s-deploy'
    file: '/gitlab-init.yml'

  - project: 'Flowhub/k8s-deploy'
    file: '/gitlab-codequality.yml'

  - project: 'Flowhub/k8s-deploy'
    file: '/gitlab-deploy.yml'
```

---

## Dockerfiles

The Docker container is a *wasp-waist* or hourglass point of abstraction

There may be a lot of complexity in the application runtime (libraries, drivers, compile dependencies) and a lot of complexity in running it (self-healing, scaling, I/O) but by building down to a Docker Image you can manage interaction between the two.

---

![the sre hourglass](images/wasp-waist.jpeg)

---

Each service started with a working Dockerfile in the root of the repo.

Developers were free to modify them as needed.

We tried to have everyone inherit from upstream and not worry about creating a "walled garden" of images.

[Dockerfile example](assets/Dockerfile-example)

---

## "How did you deal with breaking changes?"

Fork the repo and deploy as a new service.

---

## Part 2
### Simplify and Standardize for Speed and Scale

---

> "Your engineers are currently used to picking the best tool for the job by optimizing locally.  What you need to do is start building your muscles for optimizing globally.  Not in isolation of other considerations, but in conjunction with them.  It will always be a balancing act between optimizing locally for the problem at hand and optimizing globally for operability and general sanity."
> 
~ Charity Majors, [Softawre Sprawl and the Golden Path](https://charity.wtf/2018/12/02/software-sprawl-the-golden-path-and-scaling-teams-with-agency/)

---

## The Power of Being (Mostly) Monoglot

- Backend Services and APIs: Node.js
- Frontend: React
- Data Science: Python
- iOS: Swift (later React Native)
- Relational DB: Postgres
- NoSQL DB: Mongo Atlas
- Look-aside cache: Singleton Redis
- Event Bus: Google Pub/Sub
- Authentication: Auth0

---

![software sprawl](images/logging.jpg)

---

## Service Templates created a "Golden Path" 

If a developer wanted to use GraphQL instead of REST, or write a new service in Rust, they would lose the CICD and Observability and On-Call support they normally got "for free"... and would be on the hook for it themselves.

---

## Git Flow? Get F*******!

Every push to every branch triggered a new build and deployment to the dev environment.

That churn was important. To understand quickly if you broke something people were depending on, and code defensively.

These builds were named using the first eight characters of the corresponding git commit:

```
build_app:
  stage: build
  script:
    - sed -i -e "s|gitlab.com/Flowhub|gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/Flowhub|g" package.json
    - echo "{\"version\":\"${SHORT_SHA}\"}" > version.json
    - docker build -t ${CI_REGISTRY_IMAGE}:${SHORT_SHA} -t ${CI_REGISTRY_IMAGE}:latest .
    - docker push ${CI_REGISTRY_IMAGE}
    - echo "Published ${CI_REGISTRY_IMAGE}:${SHORT_SHA}"
```

---

