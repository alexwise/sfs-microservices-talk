Build the slides by running the following (based on https://gist.github.com/jsoma/629b9564af5b1e7fa62d0a3a0a47c296)

# Install pandoc
`brew install pandoc`

# Build
`pandoc -t revealjs -s -o index.html slides.md -V revealjs-url=https://unpkg.com/reveal.js/ --include-in-header=slides.css -V theme=serif`

`open index.html`